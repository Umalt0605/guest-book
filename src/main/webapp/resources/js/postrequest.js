$(document).ready(function () {
    var url = window.location;

    // submit form
    $("#commentSendForm").submit(function (event) {
        event.preventDefault();
        ajaxPost();
    });

    function ajaxPost() {

        var jsId = document.cookie.match(/JSESSIONID=[^;]+/);
        if(jsId != null) {
            if (jsId instanceof Array)
                jsId = jsId[0].substring(11);
            else
                jsId = jsId.substring(11);
        }

        var formData = {
            userName : document.getElementById("userName").value.toString(),
            userEmail : document.getElementById("userEmail").value.toString(),
            text : document.getElementById("text").value.toString(),
            sessionId : jsId,
            date : new Date()
        }

        // Do POST
        $.ajax({
            type : "POST",
            url : url,
            data : formData,
            success : function(data) {
                console.log("success");
            },
            error : function(e) {
                alert("Error!");
                console.log("ERROR: ", e);
            }
        });

        resetData();
    }

    function resetData(){
        $("#userName").val("");
        $("#userEmail").val("");
        $("#text").val("");
    }
})