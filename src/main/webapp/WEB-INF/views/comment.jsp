<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<html>

<head>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="${contextPath}/resources/js/postrequest.js"></script>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
</head>

<body>

    <h3>Welcome to the GuestBook !!!</h3>

    <c:forEach items="${comments}" var="comment">
        <table class="form-send-comment">
            <tr>
                <td><h5><span class="label label-default">User name</span></h5></td>
                <td><h5>${comment.userName}</h5></td>
            </tr>
            <tr>
                <td><h5><span class="label label-default">User Email</span></h5></td>
                <td><h5>${comment.userEmail}</h5></td>
            </tr>
            <tr>
                <td><h5><span class="label label-default">Text</span></h5></td>
                <td><h5>${comment.text}</h5></td>
            </tr>
            <tr>
                <td><h5><span class="label label-default">Date</span></h5></td>
                <td><h5>${comment.date}</h5></td>
            </tr>
        </table>
    </c:forEach>

    <form:form id="commentSendForm" name="commentSendForm" method="POST" action="/comment" modelAttribute="addCommentForm" class="form-send-comment">
        <div class="form-group">
            <input id="userName" path="userName" class="form-control" placeholder="Enter your username" autofocus="true"/>
        </div>

        <div class="form-group">
            <input id="userEmail" path="userEmail" type="email" class="form-control" aria-describedby="emailHelp" placeholder="Enter your E-mail"/>
            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
        </div>

        <div class="form-group">
            <textarea id="text" class="form-control" rows="3"></textarea>
        </div>

        <button class="btn btn-lg btn-primary btn-block" type="submit">Send</button>
    </form:form>


</body>

</html>