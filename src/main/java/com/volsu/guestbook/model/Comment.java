package com.volsu.guestbook.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by umalt on 26.07.2017
 */

@Entity
@Table(name = "comments")
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String sessionId;

    private String userName;
    private String userEmail;
    private String text;

    private Date date;

    public Comment() {}

    public Comment(String text, String userName, String userEmail, String sessionId, Date date) {
        super();
        this.userName = userName;
        this.userEmail = userEmail;
        this.sessionId = sessionId;
        this.text = text;
        this.date= date;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }
}
