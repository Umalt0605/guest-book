package com.volsu.guestbook.repository;

import com.volsu.guestbook.model.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by umalt on 26.07.2017
 */
@Repository
public interface CommentRepository extends JpaRepository<Comment, Integer> {

    @Query(value = "select * from comments order by date desc", nativeQuery = true)
    List<Comment> findAllOrderByDate();
}
