package com.volsu.guestbook.service.impl;

import com.volsu.guestbook.model.Comment;
import com.volsu.guestbook.repository.CommentRepository;
import com.volsu.guestbook.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;

import java.util.Date;
import java.util.List;

/**
 * Created by umalt on 27.07.2017
 */
@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentRepository commentRepository;

    @Override
    public void add(Comment comment) {
        comment.setUserName(comment.getUserName());
        comment.setUserEmail(comment.getUserEmail());
        comment.setText(comment.getText());
        comment.setDate(new Date());
        comment.setSessionId(RequestContextHolder.currentRequestAttributes().getSessionId());

        commentRepository.save(comment);
    }

    @Override
    public Comment editComment(Comment comment) {
        return null;
    }

    @Override
    public void delete(int id) {
        commentRepository.delete(id);
    }

    @Override
    public List<Comment> getAll() {
        return commentRepository.findAllOrderByDate();
    }
}
