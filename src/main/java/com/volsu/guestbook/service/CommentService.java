package com.volsu.guestbook.service;

import com.volsu.guestbook.model.Comment;

import java.util.List;

/**
 * Created by umalt on 27.07.2017
 */

public interface CommentService {

    void add(Comment comment);

    Comment editComment(Comment comment);

    void delete(int id);

    List<Comment> getAll();
}
