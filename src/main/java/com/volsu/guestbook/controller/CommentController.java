package com.volsu.guestbook.controller;

import com.volsu.guestbook.model.Comment;
import com.volsu.guestbook.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by umalt on 26.07.2017
 */
@Controller
public class CommentController {

    @Autowired
    private CommentService commentService;

    @RequestMapping(value = "/")
    public String index() {
        return "redirect:/comment";
    }

    @RequestMapping(value = "/comment", method = RequestMethod.GET)
    public String comment(Model model) {
        model.addAttribute("addCommentForm", new Comment());
        model.addAttribute("comments", commentService.getAll());

        return "comment";
    }

    @RequestMapping(value = "/comment", method = RequestMethod.POST)
    public String comment(@ModelAttribute("addCommentForm") Comment comment, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "redirect:/errorPage";
        }

        commentService.add(comment);

        return "comment";
    }

    @RequestMapping(value = "/errorPage")
    public String error() {
        return "errorPage";
    }
}
